# Agents Group Bingo

A motivational tool to encourage people to pay attention in Agents Group meetings.

http://amy.so/bingo

## Reuse

Feel free to take the code. You can easily make your own bingo by changing the events in the object at the start of bingo.js.

## TODO

* Persistance of scores (for players and events). Probably using couchdb. Or Taffy?
* Display and live update scores of everyone else playing at the same time.