(function(){

	var data = {
		"events": [
					"Someone invokes incomprehensible maths"
				   ,"MR leaves early to go to Mosque"
				   ,"MR <em>doesn't</em> leave early to go to Mosque"
				   ,"GAME THEORY"
				   ,"MR and AL argue for longer than 5 mins"
				   ,"MR and AL argue for longer than 10 mins"
				   ,"MR and AL argue for longer than 15 mins"
				   ,"MR makes passive aggressive / third person criticism"
				   ,"AL mentions Settlers of Catan"
				   ,"Someone implies project is too ambitious"
				   ,"Someone implies project is too trivial"
				   ,"Someone mentions Settlers to help AL understand their point"
				   ,"Presentation never ends"
				   ,"Where's the pig?"
				   ,"Someone from 2.35 is late"
				   ,"Intro runs over"
				   ,"AL or MR bitch about admin work or marking"
				   ,"AL stands up for presenter"
				   ,"AL and/or MR take over discussion entirely"
				   ,"Algorithms vs humans"
				   ,"Too abstract / need examples"
				   ,"Not abstract enough / need to generalise"
				   ,"AL interrupts MR"
				   ,"MR interrupts AL"
				   ,"MR talks about power, control or deception."
				   ,"Turmeric."
				   ,"Evaluation?"
				   ,"MR is slightly hostile towards one of his students or postdocs"
				   ,"You want to leave early but daren't"
				   ,"You give up and leave early"
				   ,"Someone new turns up and actually talks during Q&A"
				   ,"Someone playing AGBingo bursts out laughing"
				   ,"Conference bragging"
				   ,"General academic snobbery"
				   ,"Really daft examples (eg. potato-based agents)"
				   ,"Talk seems like it's finished but just keeps going"
		],
		"game": {
			"name": "",
			"events": [],
			"date": ""
		}
	};

	function init(){
		setup();
		var save_name = document.getElementById("save");
		save.addEventListener('click', function(event){
			event.preventDefault();
			storeName();
		})
		var reload = document.getElementById("reload");
		reload.addEventListener('click', function(){
			setup();
			updateScore();
		})
	}

	function setup() {
		data.game.date = new Date();
		data.game.events = [];
		console.log(data.game);
		var events = data.events;
		events.sort(function() {return 0.5 - Math.random()});
		var set = events.slice(0,9);
		var squares = document.getElementById("grid").getElementsByClassName("w1of3");
		for(var i=0; i < squares.length; i++){
			squares[i].getElementsByTagName("p")[0].removeClass('color1-bg');
			squares[i].getElementsByTagName("p")[0].innerHTML = set[i];
			squares[i].removeEventListener('click', score);
			squares[i].addEventListener('click', score);
		}
	}

	var score = function(event){
		ele = event.target;
		ele.className = ele.className + " color1-bg";
		this.removeEventListener('click', score);
		data.game.events.push(ele.innerHTML);
		updateScore();
		console.log(data.game);
	}

	function updateScore(){
		if(data.game.events.length == 9){
			document.getElementById("score").innerHTML = "BINGO!";
		}else{
			document.getElementById("score").innerHTML = data.game.events.length;
		}
	}

	function storeName(){
		var name_input = document.getElementById("name");
		name_input.addEventListener('keypress', function(e){
			name_input.removeClass("fail");
		});
		var name = name_input.value;
		name = name.trim();
		if(name == ""){
			name_input.className = name_input.className + " fail";
		}else{
			data.game.name = name;
			console.log(data.game);
			document.getElementById("namebar").innerHTML = "<span>" + name + "</span>";
		}
	}

	document.addEventListener('DOMContentLoaded', init);

})();

HTMLElement.prototype.removeClass = function(remove) {
    var newClassName = "";
    var i;
    var classes = this.className.split(" ");
    for(i = 0; i < classes.length; i++) {
        if(classes[i] !== remove) {
            newClassName += classes[i] + " ";
        }
    }
    this.className = newClassName;
}